import java.util.ArrayList;

class Group
{
    String title;
    ArrayList<Student> students;
    int num;
    Student head;
    float rating;

    public Group(String title)
    {
        this.title = title;
        this.students = new ArrayList<>();
        this.num = 0;
        this.head = null;
        this.rating = 0;
    }

    public void addStudent(Student student)
    {
        this.students.add(student);
        this.num = students.size();
    }

    public void setHead()
    {
        Student candidat = new Student(0, "");
        for (Student student : students) {
            if (student.rating > candidat.rating) candidat = student;
        }
        this.head = candidat;
    }

    public Student findStudent(String fio)
    {
        for (Student student : students)
            if (student.fio.equals(fio)) return student;
        return null;
    }

    public Student findStudent(Integer id)
    {
        for (Student student : students)
            if (student.id == (id)) return student;
        return null;
    }

    public void calcAverageMark()
    {
        int summ = 0;
        for (Student student : students) summ += student.rating;
        rating = (float) summ / students.size();
    }

    public void excludeStudent(Student student)
    {
        students.remove(student);
        num--;
        student.group = null;
        if (student == head) setHead();
    }
}
