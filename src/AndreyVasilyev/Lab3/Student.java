import java.util.ArrayList;

class Student
{
    final int id;
    final String fio;
    Group group;
    ArrayList<Integer> marks;
    int num;
    float rating;

    public Student(int id, String fio)
    {
        this.id = id;
        this.fio = fio;
        this.marks = new ArrayList<>();
        this.num = 0;
        this.group = null;
        this.rating = 0;
    }

    public void setGroup(Group group)
    {
        this.group = group;
    }

    public void addNum(int num)
    {
        marks.add(num);
    }

    public void calcAverageMark()
    {
        int summ = 0;
        for (int i : marks) summ += i;
        rating = (float) summ / marks.size();
    }
}
