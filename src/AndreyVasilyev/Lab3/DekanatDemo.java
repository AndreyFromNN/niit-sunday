public class DekanatDemo
{

    public static void main(String[] args)
    {

        Dekanat dekanat = new Dekanat();
        dekanat.spreadStudentsToGroups();
        dekanat.addMarks();
        dekanat.getStatistic();
        dekanat.chooseHeads();
        dekanat.printToConsole();
        dekanat.transferStudents();
        dekanat.excludeForBadAchievement();
        dekanat.getStatistic();
        dekanat.printToConsole();
        dekanat.transferStudents();
        dekanat.getStatistic();
        dekanat.printToConsole();
        dekanat.saveInfo();
    }
}

