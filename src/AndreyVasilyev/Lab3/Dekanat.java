import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;


class Dekanat
{
    ArrayList<Student> students;
    ArrayList<Group> groups;

    public Dekanat()
    {
        this.students = getStudents();
        this.groups = getGroups();
    }

    public ArrayList<Student> getStudents()
    {
        students = new ArrayList();
        JSONArray studentsJSON = null;
        JSONObject studentInJSON;
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("src/students.json")))) {
            String inputString;
            while ((inputString = in.readLine()) != null) {
                stringBuilder.append(inputString);
            }
            studentsJSON = (JSONArray) JSONValue.parseWithException(stringBuilder.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for (Object jsonObject : studentsJSON) {
            studentInJSON = (JSONObject) jsonObject;
            int id = Integer.parseInt(studentInJSON.get("id").toString());
            String fio = (String) studentInJSON.get("fio");
            students.add(new Student(id, fio));
        }
        return students;
    }

    public ArrayList<Group> getGroups()
    {
        groups = new ArrayList();
        JSONArray groupsJSON = null;
        JSONObject groupInJSON;
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("src/groups.json")))) {
            String inputString;
            while ((inputString = in.readLine()) != null) {
                stringBuilder.append(inputString);
            }
            groupsJSON = (JSONArray) JSONValue.parseWithException(stringBuilder.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for (Object jsonObject : groupsJSON) {
            groupInJSON = (JSONObject) jsonObject;
            String title = groupInJSON.get("title").toString();
            groups.add(new Group(title));
        }
        return groups;
    }

    public void spreadStudentsToGroups()
    {
        int groupID;
        for (Student student : students) {
            groupID = (int) (Math.random() * groups.size());
            groups.get(groupID).addStudent(student);
            student.setGroup(groups.get(groupID));
        }
    }

    public void addMarks()
    {
        for (Student student : students) {
            int numberOfMarks = (int) ((Math.random() * 20) + 1);
            student.num = numberOfMarks;
            for (int i = 0; i < numberOfMarks; i++) {
                student.addNum((int) ((Math.random() * 5) + 1));
            }
        }
    }

    public void getStatistic()
    {
        for (Student student : students) student.calcAverageMark();
        for (Group group : groups) group.calcAverageMark();
    }

    public void transferStudents()
    {
        int numberOfStudents = (int) (Math.random() * students.size());
        int rndStud;
        int newGroup;
        for (int i = 0; i < numberOfStudents; i++) {
            rndStud = (int) ((Math.random() * students.size()));
            newGroup = (int) (Math.random() * groups.size());
            if (students.get(rndStud).group == groups.get(newGroup))
                continue;
            students.get(rndStud).group.excludeStudent(students.get(rndStud)); //remove from group
            groups.get(newGroup).students.add(students.get(rndStud)); //move to new group
            students.get(rndStud).setGroup(groups.get(newGroup)); //change group in the student field's group = groups.get(newGroup)
        }
    }

    public void excludeForBadAchievement()
    {
        Iterator<Student> inspectedStudentIter = students.iterator();
        while (inspectedStudentIter.hasNext()) {
            Student inspectedStudent = inspectedStudentIter.next();
            if (inspectedStudent.rating < 3) {
                inspectedStudent.group.excludeStudent(inspectedStudent);
                inspectedStudentIter.remove();
            }
        }
    }

    public void saveInfo()
    {
        JSONObject groupInJSON;
        JSONObject studentInJSON;
        JSONArray groupsJSON = new JSONArray();
        JSONArray studentsJSON = new JSONArray();

        for (Student student : students) {
            studentInJSON = new JSONObject();
            studentInJSON.put("id", student.id);
            studentInJSON.put("fio", student.fio);
            studentInJSON.put("group", student.group.title);
            studentInJSON.put("marks", student.marks);
            studentInJSON.put("num", student.num);
            studentInJSON.put("rating", student.rating);
            studentsJSON.add(studentInJSON);
        }

        try {
            BufferedWriter writeToFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/students.json")));
            writeToFile.write(studentsJSON.toJSONString());
            writeToFile.flush();
            writeToFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Group group : groups) {

            groupInJSON = new JSONObject();
            groupInJSON.put("title", group.title);
            groupInJSON.put("students", group.students);
            groupInJSON.put("num", group.num);
            groupInJSON.put("head", group.head);
            groupInJSON.put("rating", group.rating);
            groupsJSON.add(groupInJSON);
        }

        try {
            BufferedWriter writeToFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("src/groups.json")));
            writeToFile.write(groupsJSON.toJSONString());
            writeToFile.flush();
            writeToFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void chooseHeads()
    {
        for (Group group : groups) group.setHead();
    }

    public void printToConsole()
    {
        for (Group group : groups) {
            System.out.println("--------------------------------------------------------------------------------------------------------------------------");
            System.out.println("Название группы: " + group.title + ", количество студентов: " + group.num + ", староста: " + group.head.fio + ", средний балл группы: " + group.rating);
            System.out.println("--------------------------------------------------------------------------------------------------------------------------");

            for (Student student : group.students) {
                System.out.println("ФИО студента: " + student.fio + ", ID студента: " + student.id + ", средний балл: " + student.rating + ", группа: " + student.group.title);
            }

        }
    }
}

