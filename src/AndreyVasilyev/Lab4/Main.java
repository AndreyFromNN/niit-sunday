import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;

public class Main
{

    public static void main(String[] args)
    {
        Initiation.initialise();
        Accounting.calculateAllWages();
        Accounting.printAllWages();
    }
}

abstract class Employee
{
    int id;
    String name;
    float worktime;
    float base;
    float wage;
    Project project;

    public void calculateWage()
    {
    }

    public void setProject(Project project)
    {
        this.project = project;
    }
}

interface TimePayment
{
    void workTimePayment();
}

interface ProjectPayment
{
    void projectPayment();
}

interface HeadingPayment
{
    void headingPayment();
}

class Personal extends Employee implements TimePayment
{
    float worktime;

    @Override
    public void workTimePayment()
    {
        this.wage = worktime * base;
    }
}

class Engineer extends Employee implements TimePayment, ProjectPayment
{

    @Override
    public void workTimePayment()
    {

    }

    @Override
    public void projectPayment()
    {

    }
}

class Manager extends Employee implements ProjectPayment
{

    @Override
    public void projectPayment()
    {
    }
}

class Cleaner extends Personal
{
    @Override
    public void calculateWage()
    {
        workTimePayment();
    }

    public Cleaner(int id, String name, float base)
    {
        this.id = id;
        this.name = name;
        this.base = base;
        this.worktime = 160;
    }


    @Override
    public void workTimePayment()
    {
        wage += worktime * base;
    }
}

class Driver extends Personal
{

    public Driver(int id, String name, float base)
    {
        this.id = id;
        this.name = name;
        this.base = base;
        this.worktime = 160;
    }

    @Override
    public void calculateWage()
    {
        workTimePayment();
    }

    @Override
    public void workTimePayment()
    {
        wage += worktime * base;
    }
}

class Programmer extends Engineer
{
    public Programmer()
    {
    }

    public Programmer(int id, String name, float base)
    {
        this.id = id;
        this.name = name;
        this.base = base;
        this.worktime = 160;
    }

    @Override
    public void calculateWage()
    {
        workTimePayment();
        projectPayment();
    }

    @Override
    public void workTimePayment()
    {
        wage += worktime * base;
    }

    @Override
    public void projectPayment()
    {
        wage += project.projectBudget * 0.01F;
    }
}

class Tester extends Engineer
{
    public Tester(int id, String name, float base)
    {
        this.id = id;
        this.name = name;
        this.base = base;
        this.worktime = 160;
    }

    @Override
    public void calculateWage()
    {
        workTimePayment();
        projectPayment();
    }

    @Override
    public void workTimePayment()
    {
        wage += worktime * base;
    }

    @Override
    public void projectPayment()
    {
        wage += project.projectBudget * 0.01F;
    }
}

class TeamLeader extends Programmer implements HeadingPayment
{


    public TeamLeader(int id, String name, float base)
    {
        this.id = id;
        this.name = name;
        this.base = base;
        this.worktime = 160;
    }

    @Override
    public void calculateWage()
    {
        headingPayment();
        workTimePayment();
        projectPayment();
    }

    @Override
    public void headingPayment()
    {
        int inferiorsNumber = 0;
        for (Project prj : Accounting.projects) {
            inferiorsNumber += prj.projectEngineers.size();
        }
        wage += inferiorsNumber * 250;
    }

    @Override
    public void workTimePayment()
    {
        wage += worktime * base;
    }

    @Override
    public void projectPayment()
    {
        int summaryBudget = 0;
        for (Project prj : Accounting.projects) {
            summaryBudget += prj.projectBudget;
        }
        wage += (summaryBudget * 0.01F) / Accounting.projects.size();
    }
}

class ProjectManager extends Manager implements HeadingPayment
{
    public ProjectManager()
    {
    }

    public ProjectManager(int id, String name, float base)
    {
        this.id = id;
        this.name = name;
        this.base = base;
        this.worktime = 160;
    }

    @Override
    public void calculateWage()
    {
        headingPayment();
        projectPayment();
    }

    @Override
    public void headingPayment()
    {
        wage += project.projectEngineers.size() * 350;
    }

    @Override
    public void projectPayment()
    {
        wage += project.projectBudget * 0.1F;
    }
}

class SeniorManager extends ProjectManager
{
    public SeniorManager(int id, String name, float base)
    {
        this.id = id;
        this.name = name;
        this.base = base;
    }

    @Override
    public void calculateWage()
    {
        headingPayment();
        projectPayment();
    }

    @Override
    public void headingPayment()
    {
        int inferiorsNumber = 0;
        for (Project prj : Accounting.projects) {
            inferiorsNumber += prj.projectEngineers.size();
        }
        wage += inferiorsNumber * 250;
    }

    @Override
    public void projectPayment()
    {
        int summaryBudget = 0;
        for (Project prj : Accounting.projects) {
            summaryBudget += prj.projectBudget;
        }
        wage += (summaryBudget * 0.15F) / Accounting.projects.size();
    }
}

class Project
{
    String name;
    float projectBudget;
    ArrayList<Employee> projectEngineers;

    public Project(String name, float projectBudget)
    {
        this.name = name;
        this.projectBudget = projectBudget;
        this.projectEngineers = new ArrayList<>();
    }
}

class Initiation
{
    public static void initialise()
    {
        JSONArray projectsJSON = new JSONArray();
        JSONObject projectInJSON = null;
        StringBuilder strBuilder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("src/projects.json")))) {
            String inputString;
            while ((inputString = in.readLine()) != null)
                strBuilder.append(inputString);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            projectsJSON = (JSONArray) JSONValue.parseWithException(strBuilder.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (Object proj : projectsJSON) {
            projectInJSON = (JSONObject) proj;
            String name = (String) projectInJSON.get("name");
            float budget = Float.parseFloat(projectInJSON.get("projectBudget").toString());
            Accounting.projects.add(new Project(name, budget));
        }

        JSONArray employeesJSON = new JSONArray();
        JSONObject employeeInJSON = null;
        strBuilder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("src/employees.json")))) {
            String inputString;
            while ((inputString = in.readLine()) != null)
                strBuilder.append(inputString);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            employeesJSON = (JSONArray) JSONValue.parseWithException(strBuilder.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (Object empl : employeesJSON) {
            employeeInJSON = (JSONObject) empl;
            int id = Integer.parseInt(employeeInJSON.get("id").toString());
            String name = (String) employeeInJSON.get("name");
            float base = Float.parseFloat(employeeInJSON.get("base").toString());
            String projectName = (String) employeeInJSON.get("project");
            String position = (String) employeeInJSON.get("position");
            switch (position) {
                case "cleaner":
                    Accounting.employees.add(new Cleaner(id, name, base));
                    break;
                case "driver":
                    Accounting.employees.add(new Driver(id, name, base));
                    break;
                case "programmer":
                    Accounting.employees.add(new Programmer(id, name, base));
                    Initiation.addEmplToProject(projectName);
                    break;
                case "tester":
                    Accounting.employees.add(new Tester(id, name, base));
                    Initiation.addEmplToProject(projectName);
                    break;
                case "teamleader":
                    Accounting.employees.add(new TeamLeader(id, name, base));
                    Initiation.addEmplToProject(projectName);
                    break;
                case "projectmanager":
                    Accounting.employees.add(new ProjectManager(id, name, base));
                    Initiation.addEmplToProject(projectName);
                    break;
                case "seniormanager":
                    Accounting.employees.add(new SeniorManager(id, name, base));
                    Initiation.addEmplToProject(projectName);
                    break;
            }
        }
    }

    static void addEmplToProject(String projectName)
    {

        for (Project project : Accounting.projects) {
            if (project.name.equals(projectName)) {
                project.projectEngineers.add(Accounting.employees.get(Accounting.employees.size() - 1));
                Accounting.employees.get(Accounting.employees.size() - 1).setProject(project);
            }
        }
    }
}

class Accounting
{

    public static ArrayList<Employee> employees = new ArrayList<>();
    public static ArrayList<Project> projects = new ArrayList<>();

    public static void calculateAllWages()
    {
        for (Employee empl : employees) {
            empl.calculateWage();
        }
    }

    public static void printAllWages(){
        System.out.format("%-25s%-20s%-20s%n", "Employee name", "Employee position", "Employee wage");
        System.out.println("------------------------------------------------------------");
        for (Employee empl : employees) System.out.format("%-25s%-20s%-20s%n", empl.name, empl.getClass().getName(), empl.wage);
    }

}