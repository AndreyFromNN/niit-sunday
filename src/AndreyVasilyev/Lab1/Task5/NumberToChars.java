
import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStreamReader;
        import java.util.HashMap;

public class NumberToChars {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String numbers = reader.readLine();
        HashMap<Character, HashMap<Integer,String>> chars = new HashMap<>();
        HashMap<Integer,String> charsFor0 = new HashMap<>();
        charsFor0.put(0,"   ***  ");
        charsFor0.put(1,"  *   * ");
        charsFor0.put(2," *     *");
        charsFor0.put(3," *     *");
        charsFor0.put(4," *     *");
        charsFor0.put(5,"  *   * ");
        charsFor0.put(6,"   ***  ");
        chars.put('0', charsFor0);
        HashMap<Integer,String> charsFor1 = new HashMap<>();
        charsFor1.put(0,"  * ");
        charsFor1.put(1," ** ");
        charsFor1.put(2,"  * ");
        charsFor1.put(3,"  * ");
        charsFor1.put(4,"  * ");
        charsFor1.put(5,"  * ");
        charsFor1.put(6," ***");
        chars.put('1', charsFor1);
        HashMap<Integer,String> charsFor2 = new HashMap<>();
        charsFor2.put(0,"  *** ");
        charsFor2.put(1," *   *");
        charsFor2.put(2," *  * ");
        charsFor2.put(3,"   *  ");
        charsFor2.put(4,"  *   ");
        charsFor2.put(5," *    ");
        charsFor2.put(6," *****");
        chars.put('2', charsFor2);
        HashMap<Integer,String> charsFor3 = new HashMap<>();
        charsFor3.put(0,"  *** ");
        charsFor3.put(1," *   *");
        charsFor3.put(2,"     *");
        charsFor3.put(3,"   ** ");
        charsFor3.put(4,"     *");
        charsFor3.put(5," *   *");
        charsFor3.put(6,"  *** ");
        chars.put('3', charsFor3);
        HashMap<Integer,String> charsFor4 = new HashMap<>();
        charsFor4.put(0,"    *  ");
        charsFor4.put(1,"   **  ");
        charsFor4.put(2,"  * *  ");
        charsFor4.put(3," *  *  ");
        charsFor4.put(4," ******");
        charsFor4.put(5,"    *  ");
        charsFor4.put(6,"    *  ");
        chars.put('4', charsFor4);
        HashMap<Integer,String> charsFor5 = new HashMap<>();
        charsFor5.put(0," **** ");
        charsFor5.put(1," *    ");
        charsFor5.put(2," *    ");
        charsFor5.put(3," **** ");
        charsFor5.put(4,"     *");
        charsFor5.put(5," *   *");
        charsFor5.put(6,"  *** ");
        chars.put('5', charsFor5);
        HashMap<Integer,String> charsFor6 = new HashMap<>();
        charsFor6.put(0,"  *** ");
        charsFor6.put(1," *    ");
        charsFor6.put(2," *    ");
        charsFor6.put(3," **** ");
        charsFor6.put(4," *   *");
        charsFor6.put(5," *   *");
        charsFor6.put(6,"  *** ");
        chars.put('6', charsFor6);
        HashMap<Integer,String> charsFor7 = new HashMap<>();
        charsFor7.put(0," *****");
        charsFor7.put(1,"     *");
        charsFor7.put(2,"    * ");
        charsFor7.put(3,"   *  ");
        charsFor7.put(4,"  *   ");
        charsFor7.put(5," *    ");
        charsFor7.put(6," *    ");
        chars.put('7', charsFor7);
        HashMap<Integer,String> charsFor8 = new HashMap<>();
        charsFor8.put(0,"  *** ");
        charsFor8.put(1," *   *");
        charsFor8.put(2," *   *");
        charsFor8.put(3,"  *** ");
        charsFor8.put(4," *   *");
        charsFor8.put(5," *   *");
        charsFor8.put(6,"  *** ");
        chars.put('8', charsFor8);
        HashMap<Integer,String> charsFor9 = new HashMap<>();
        charsFor9.put(0,"  ****");
        charsFor9.put(1," *   *");
        charsFor9.put(2," *   *");
        charsFor9.put(3,"  ****");
        charsFor9.put(4,"     *");
        charsFor9.put(5,"     *");
        charsFor9.put(6,"     *");
        chars.put('9', charsFor9);

        for(int i = 0; i < 7; i++)
        {
            StringBuilder stringToPrint = new StringBuilder();
            for (int j = 0; j < numbers.length(); j++)
            {
                char index = numbers.charAt(j);
                HashMap temp = new HashMap();
                temp = chars.get(index);
                stringToPrint.append(temp.get(i));
            }
            System.out.println(stringToPrint);
        }


    }
}
